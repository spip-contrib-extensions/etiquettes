<?php

/**
 * Plugin  : Étiquettes
 * Auteur  : RastaPopoulos
 * Licence : GPL
 *
 * Documentation : https://contrib.spip.net/Plugin-Etiquettes
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Insertion des CSS
 * @param $flux
 * @return string
 */
function etiquettes_insert_head_css($flux) {

	$css = timestamp(find_in_path('css/etiquettes.css'));
	$flux .= '<link rel="stylesheet" type="text/css" media="all" href="' . $css . '" />';

	return $flux;
}

/**
 * Insertion des JS
 * @param $flux
 * @return string
 */
function etiquettes_insert_head($flux) {

	$js = timestamp(find_in_path('javascript/etiquettes.js'));
	$flux .= '<script type="text/javascript" src="' . $js . '"></script>';

	return $flux;
}

/**
 * Retourner la position de &quot; dans une chaine ou -1 si absent
 * @param string $valeur
 * @return int
 */
function etiquettes_position_quot($valeur) {
	$position = strpos($valeur, '&quot;');
	return ($position === false ? -1 : $position);
}

/**
 * @deprecated
 * @param $texte
 * @param $id
 * @param $groupe_defaut
 * @param $type
 * @param $id_type
 * @param $clear
 * @return void
 */
function ajouter_etiquettes($texte, $id, $groupe_defaut = 'tags', $type = 'documents', $id_type = 'id_document', $clear = false) {

	include_spip('inc/tag-machine');
	$clear = ($clear == 'true');
	ajouter_liste_mots($texte, $id, $groupe_defaut, $type, $id_type, $clear);
}
